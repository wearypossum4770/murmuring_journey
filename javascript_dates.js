/** @format */

const d1 = new Date("2017-06-01");
const d2 = new Date("2018-06-01");
const d3 = new Date("2019-06-01");

let dates = [d2, d1, d3].sort((a, b) => a - b);
console.log(dates);
console.log(d1 - d3); // 0
console.log(d1 - d2); // 1 year in milliseconds, 1000 * 60 * 60 * 24 * 365
console.log(d1.toString());
console.log(d1.valueOf()); // 1559347200000

console.log(d1.toString() === d2.toString()); // false
console.log(d1.toString() === d3.toString()); // true

console.log(d1.valueOf() === d2.valueOf()); // false
console.log(d1.valueOf() === d3.valueOf()); // true

function isObject(arg) {
  if (arg == null || typeof arg !== "object") {
    return false;
  }
  const proto = Object.getPrototypeOf(arg);
  if (proto == null) {
    return true; // `Object.create(null)`
  }
  return proto === Object.prototype;
}
function toBinary(v, str) {
  if (!Number.isSafeInteger(v) || v < 0) {
    throw new Error("v must be a non-negative integer");
  }
  if (v === 1) {
    return "1";
  }
  if (v === 0) {
    return "0";
  }
  return toBinary(Math.floor(v / 2)) + (v % 2);
}
