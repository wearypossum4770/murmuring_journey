from django.core.validators import MaxLengthValidator
from django.db.models import CharField, Model, PositiveSmallIntegerField


class Course(Model):

    course_code = CharField()
    teaches = CharField()
    number_of_credits = PositiveSmallIntegerField(validators=[MaxLengthValidator(10)])
    # course_prerequisites =
    # course_mode = CharField(max_length=3, choices=Mode.choices)
