from pathlib import Path

from django.contrib.auth import get_user_model
from django.db.models import (
    CASCADE,
    CharField,
    DateTimeField,
    FileField,
    ForeignKey,
    Model,
)

from todos.models import Todo

User = get_user_model()


class Attachment(Model):
    title = CharField(max_length=100, null=True, blank=False)
    content_location = FileField(upload_to="unprocessed_uploads/")
    date_modified = DateTimeField(auto_now=True)
    date_created = DateTimeField(auto_now_add=True)
    associated_todo = ForeignKey(Todo, on_delete=CASCADE, null=True, blank=False)
    # citation
    # thumbnail_url
    description = CharField(max_length=100, null=True, blank=False)
    author
digitized_date
disposition_date
file_format
location
access_restriction
keyword
    def __str__(self):
        return self.description

    @property
    def extension(self):
        return self.filename_info.suffix

    @property
    def filename(self):
        return self.filename_info.stem

    @property
    def filename_info(self):
        return Path(self.content_location.path)


# convert -scale '800x800+0+0>' -colorspace rgb -strip in.pdf[0] out.png

# gs -q -dBATCH -dNOPAUSE -sDEVICE=pngalpha -dMAxBitmap=500000000 -dAlignToPixles=0 -dGridFitTT=0 -r150x150 -sOutputFile=out.png in.pdf
