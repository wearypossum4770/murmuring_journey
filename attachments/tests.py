import pytest
from django.contrib.auth import get_user_model
from django.test import TestCase

from attachments.models import Attachment

pytestmark = pytest.mark.django_db


class TestAttachments(TestCase):
    fixtures = ("todoinit.json",)

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.attachment_list = Attachment.objects.all()
        cls.attachment = cls.attachment_list[0]

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def test_attachment_models_loads(self):
        assert len(self.attachment_list) > 0

    def test_attachment_todo_relationship(self):
        assert self.attachment.associated_todo_id == 21

    def test_attachment_filename(self):
        assert self.attachment.filename == "example"

    def test_attachment_extension(self):
        assert self.attachment.extension == ".pdf"
