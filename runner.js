/** @format */

import { readFileSync, writeFileSync } from "fs";
let fileContents = JSON.parse(readFileSync("./src/data/database.json"));
let data = JSON.parse(readFileSync("./data.json"));
let arr = [];
data.forEach(d => {
  if (d.model === "") {
    arr.push({});
  }
});
let obj = { ...fileContents };
writeFileSync("./src/data/database.json", JSON.stringify(obj));
console.log(obj);
