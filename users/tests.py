import pytest
from django.contrib.auth import get_user_model
from django.test import Client, TestCase

from users.models import Profile
from users.tasks import get_user_profiles, remove_previously_processed

pytestmark = pytest.mark.django_db


# @pytest.fixture(scope="session")
# class TestClery(TestCase):
#     @classmethod
#     def setUpClass(cls):
#         super().setUpClass()
#     def image_file(tmpdir_factory):
#         img = compute_expensive_image()
#         fn = tmpdir_factory.mktemp("data").join("img.png")
#         img.save(str(fn))
#         return fn
#     @classmethod
#     def tearDownClass(cls):
#         super().tearDownClass()


pytestmark = pytest.mark.django_db

davidattenborough = {"username": "davidattenborough", "password": "boatymcboatface"}


class TestProfile(TestCase):
    fixtures = ("todoinit.json",)

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.client = Client()
        cls.sir_attenborough = get_user_model().objects.get(last_name="attenborough")
        cls.profiles = Profile.objects.all()
        cls.obama = Profile.objects.first()

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def test_profile_list(self):
        assert len(self.profiles) > 0

    def test_first_name(self):
        assert self.obama.user.first_name == "barack"

    def test_last_name(self):
        assert self.obama.user.last_name == "obama"

    def test_username(self):
        assert self.obama.user.username == "barack.hussein.obama"

    def test_email(self):
        assert self.obama.user.email == "barack.hussein.obama@president.usgov.com"

    def test_middle_name(self):
        assert self.obama.middle_name == "hussein"

    def test_pronouns(self):
        assert self.obama.pronouns == "HE"

    def test_headline(self):
        assert self.obama.headline == "44th president of the United States"

    def test_get_user_profiles(self):
        profile_list = get_user_profiles()
        assert len(profile_list) == 4

    def test_profile_image_processed_before(self):
        assert self.obama.profile_image_processed == False

    def test_removed_previously_processed_images(self):
        assert sum(1 for _ in remove_previously_processed(self.profiles)) == 1

    def test_previous_image_location(self):
        ...

    def test_bio(self):
        assert (
            self.obama.bio
            == "is an American politician and attorney who served as the 44th president of the United States from 2009 to 2017. A member of the Democratic Party, Obama was the first African-American president of the United States. He previously served as a U.S. senator from Illinois from 2005 to 2008 and as an Illinois state senator from 1997 to 2004."
        )

    def test_user_api_access(self):
        pass

    def test_david_attenborough_access_token(self):
        response = self.client.post("/api/token/", davidattenborough)
        assert response.data.get("access") is not None
