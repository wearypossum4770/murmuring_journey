from os import rename
from pathlib import Path
from subprocess import CalledProcessError, check_output, run

from django.conf import settings
from django.contrib.auth import get_user_model
from PIL import Image

from users.models import Profile


def is_webp_installed():
    try:
        response = check_output(
            (
                "dpkg",
                "-s",
                "webp",
            )
        )
    except CalledProcessError:
        check_output(("sudo", "apt", "upgrade", "-y"))


def get_user_profiles(*args, **kwargs):
    """
    Retrieves all user profiles.
    """
    return Profile.objects.all()


def remove_previously_processed(profiles, *args, **kwargs):
    """
    Generator returns non-processed profile images.
    """
    try:
        return (profile for profile in profiles if profile.profile_image_processed)
    except StopIteration:
        pass


def image_pre_processing_location(profiles, *args, **kwargs):
    """
    Retrieves location of user's profile image.
    """
    try:
        return (
            (
                user_profile.username,
                user_profile.profile.image.path,
            )
            for user_profile in profiles
        )
    except StopIteration:
        pass


def image_post_processing_location(profile=None, *args, **kwargs):
    """
    Forcefully creates profile image path.
    """
    new_path = kwargs.get("new_path")
    if new_path is None:
        new_path = profile.user.username
    Path(f"{settings.MEDIA_ROOT}/profile_pictures/{new_path}").mkdir(
        parents=True, exist_ok=True
    )


def process_profile_image(*args, **kwargs):
    profiles = get_user_profiles()
    pre_processed_profiles = [
        (
            user_profile.username,
            user_profile.profile.image.path,
        )
        for user_profile in profiles
        if user_profile.profile_image_processed
    ]
    for user_profile in profiles:
        if user_profile.profile_image_processed == False:
            initial_path = user_profile.profile.image.path
            image_obj = Path(initial_path)
            run("convert", initial_path, f"{image_obj.parent}/{image_obj.name}.jpg")
            new_path = user_profile.username
            Path(f"{settings.MEDIA_ROOT}/profile_pictures/{new_path}").mkdir(
                parents=True, exist_ok=True
            )
            rename(initial_path, new_path)
        user_profile.save()
