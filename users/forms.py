from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm
from django.forms import EmailField, ModelForm

from users.models import Profile

User = get_user_model()


class UserRegisterForm(UserCreationForm):
    email = EmailField()

    class Meta:
        model = User
        fields = (
            "username",
            "email",
            "password",
            "first_name",
            "last_name",
        )


class UserUpdateForm(ModelForm):
    email = EmailField()

    class Meta:
        model = User
        fields = (
            "first_name",
            "last_name",
            "password",
        )


class ProfileUpdateForm(ModelForm):
    class Meta:
        model = Profile
        fields = (
            "middle_name",
            "bio",
            "headline",
            "image",
        )
