import json
from collections import defaultdict

from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ValidationError
from django.http import JsonResponse
from django.shortcuts import render
from rest_framework.decorators import api_view

from todos.models import Todo
from users.models import Profile

User = get_user_model()


def handle_user_profile(**kwargs):
    user = User.objects.create_user(
        first_name=kwargs.data.get("first_name"),
        last_name=kwargs.data.get("last_name"),
        email=kwargs.data.get("email"),
        username=kwargs.data.get("username"),
        password=kwargs.data.get("password"),
    )
    profile = Profile.objects.create(
        user=user,
        middle_name=kwargs.data.get("middle_name"),
        bio=kwargs.data.get("bio"),
        headline=kwargs.data.get("headline"),
        image=kwargs.data.get("image"),
    )
    profile.full_clean()
    profile.save()
    return {"message": "SUCCESS"}


@api_view(["POST"])
def signup(request):
    content = handle_user_profile(request)
    return JsonResponse(content)


@login_required
@api_view(["GET", "POST"])
def profile(request):
    content = {}
    if request.method == "POST":
        content.update(handle_user_profile(request))
    return JsonResponse(content)


def homepage(request):
    todos = Todo.objects.all()
    statistics = defaultdict(int)
    for todo in todos:
        statistics["total"] += 1
        statistics["completed"] += todo.completed
        statistics["overdue"] += todo.is_overdue or 0
        statistics["archived"] += todo.is_archived
        statistics["currently_due"] += todo.is_due
    return render(request, "users/dashboard.html", {"statistics": dict(statistics)})
