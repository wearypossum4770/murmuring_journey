from django.contrib.auth import get_user_model
from django.db.models import (
    CASCADE,
    BooleanField,
    CharField,
    DateTimeField,
    ImageField,
    Model,
    OneToOneField,
    TextChoices,
    TextField,
)
from django.utils.translation import gettext_lazy as _

output_size = (300, 300)
User = get_user_model()


class Profile(Model):
    class Pronouns(TextChoices):
        HE = "HE", _("He/Him His, Himself")
        SHE = "SHE", _("She/Her Hers, Herself")
        THEY = "THEY", _("They/Them Theirs, themself")
        ZE = "ZE", _("Ze (or Zie or Xe)	Zee (like “see” with a “Z”)")

    user = OneToOneField(User, on_delete=CASCADE)
    middle_name = CharField(max_length=50, null=True, blank=True)
    image = ImageField(
        default="default.webp", upload_to="profile_pics", null=True, blank=True
    )
    headline = CharField(max_length=50, null=True, blank=True)
    bio = TextField(null=True, blank=True)
    pronouns = CharField(
        max_length=5,
        choices=Pronouns.choices,
        default=Pronouns.THEY,
        null=True,
        blank=True,
    )
    date_modified = DateTimeField(auto_now=True)
    date_created = DateTimeField(auto_now_add=True)
    profile_image_processed = BooleanField(default=False)

    def __str__(self):
        return f"{self.user.first_name.capitalize()} {self.user.last_name.capitalize()}'s  Profile"
