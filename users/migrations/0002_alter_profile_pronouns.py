# Generated by Django 3.2.3 on 2021-05-20 00:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("users", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="profile",
            name="pronouns",
            field=models.CharField(
                choices=[
                    ("HE", "He/Him His, Himself"),
                    ("SHE", "She/Her Hers, Herself"),
                    ("THEY", "They/Them Theirs, themself"),
                    ("ZE", "Ze (or Zie or Xe)\tZee (like “see” with a “Z”)"),
                ],
                default="THEY",
                max_length=5,
            ),
        ),
    ]
