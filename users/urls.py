from django.urls import path

from users.views import homepage, signup

urlpatterns = [
    path("", homepage, name="home"),
    path("signup/", signup, name="signup"),
]
