/** @format */

let db = [
  {
    username: "john.doe",
    password: "password123!@#",
    auth_tokens: {
      access:
        "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwidXNlcm5hbWUiOiJqb2huLmRvZSIsImFkbWluIjpmYWxzZSwianRpIjoiZTY1YzBhNWQtYjdkMC00MzcxLTgxZmMtMTA1NjdiNTI5ZWI4IiwiaWF0IjoxNjIxODgyMDc3LCJleHAiOjE2MjE4ODU2OTh9.qNJxUzE2_gp3sJ-kalDnCsscuNgQ5Lki_t9txTatdQw",
      refresh:
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX3BrIjoxLCJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImNvbGRfc3R1ZmYiOiLimIMiLCJleHAiOjIzNDU2NywianRpIjoiZGUxMmY0ZTY3MDY4NDI3ODg5ZjE1YWMyNzcwZGEwNTEifQ.aEoAYkSJjoWH1boshQAaTkf8G3yn0kapko6HFRt7Rh4",
    },
  },
  {
    username: "jane.doe",
    password: "password123!@#",
    auth_tokens: {
      access:
        "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwidXNlcm5hbWUiOiJqYW5lLmRvZSIsImFkbWluIjpmYWxzZSwianRpIjoiZTY1YzBhNWQtYjdkMC00MzcxLTgxZmMtMTA1NjdiNTI5ZWI4IiwiaWF0IjoxNjIxODgyMDc3LCJleHAiOjE2MjE4ODU3MzV9.BNXOnfBQiu6IBJx6RcQY3TbejyhI63R_anNnW4y4Kkg",
      refresh:
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX3BrIjoxLCJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImNvbGRfc3R1ZmYiOiLimIMiLCJleHAiOjIzNDU2NywianRpIjoiZGUxMmY0ZTY3MDY4NDI3ODg5ZjE1YWMyNzcwZGEwNTEifQ.aEoAYkSJjoWH1boshQAaTkf8G3yn0kapko6HFRt7Rh4",
    },
  },
];

export const users = {
  login: function (_username, _password) {
    return db.filter(
      user => user.username === _username && user.password === _password,
    );
  },
};
