/** @format */

import "../signup/signup_style.css";
import { Link, useHistory } from "react-router-dom";
import { useEffect, useReducer } from "react";
import useFetch from "../../hooks/useFetch";
import { useAuth } from "../../context/AuthContext";
const initialState = {
  url: "http://localhost:8000/api/token/",
  isLoggedIn: false,
  isError: false,
  apiUser: "",
  performFetch: false,
  authTokens: null,
  credentials: {
    username: "donald.john.tump.sr",
    password: "password123!@#",
  },
};
export default function Login(props) {
  const referer = props?.location?.state?.referer || "/dashboard";
  const { setAuthTokens } = useAuth();
  let history = useHistory();
  function reducer(state, action) {
    switch (action.type) {
      default:
        return { ...state };
      case "LOGIN":
        return { ...state, performFetch: true };
      case "CHANGE":
        return { ...state, credentials: { ...action.payload } };
      case "LOGIN_FAILURE":
        return { ...state, isError: true };
      case "LOGIN_SUCCESS":
        return { ...state, isLoggedIn: true, authTokens: action.payload };
    }
  }
  const [state, dispatch] = useReducer(reducer, initialState);
  const response = useFetch(state.url, state.performFetch, {
    mode: "cors",
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(state.credentials),
  });
  const handleLogin = async () => {
    dispatch({ type: "LOGIN" });
    if (response.ok){
      
    }

  };
  useEffect(() => {
    if (state.isLoggedIn) {
      dispatch({ type: "VOID", payload: null });
      setTimeout(history.push(referer), 1_000);
    }
    if (state.isError) {
      alert("The username or password provided were incorrect!");
    }
  });
  const handleChange = ({ target: { name, value } }) =>
    dispatch({
      type: "CHANGE",
      payload: { ...state.credentials, [name]: value },
    });
  const handleSubmit = e => {
    e.preventDefault();
    handleLogin();
  };
  return (
    <>
      <form onSubmit={handleSubmit} style={{ border: "1px solid #ccc" }}>
        <div className="container">
          <input
            id="testId-api-successful"
            htmlFor="token"
            value={state?.credentials?.username}
            hidden
          />
          <h1>Login</h1>
          <p>Please to your account.</p>
          <hr />
          <label htmlFor="email">
            <b>Email:</b>
          </label>
          <input
            autoComplete="email"
            onChange={handleChange}
            type="text"
            name="email"
          />
          <label htmlFor="password">
            <b>Password:</b>
          </label>
          <input
            onChange={handleChange}
            autoComplete="current-password"
            type="password"
            name="password"
          />
          <div className="clearfix">
            <button
              onClick={() => history.push("/")}
              type="button"
              className="cancelbtn"
            >
              Cancel
            </button>
            <button onClick={handleSubmit} type="submit" className="signupbtn">
              Sign Up
            </button>
          </div>
        </div>
      </form>
      <Link to="/signup">Don't have an account?</Link>
    </>
  );
}
