/** @format */

import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import Login from "./Login";
let container = null;

beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});
afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("logs in user", async () => {
  const credentials = {
    username: "leonardo.wilhelm.dicaprio",
    password: "password123!@#",
  };
  jest.spyOn(global, "fetch").mockImplementation(() =>
    Promise.resolve({
      json: () => Promise.resolve(credentials),
    }),
  );

  await act(async () => {
    render(<Login />, container);
  });
  expect(container.querySelector("testId-api-successful").textContent).toBe(
    credentials.username,
  );
  global.fetch.mockRestore();
});
