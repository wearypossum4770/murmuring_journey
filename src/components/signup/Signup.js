/** @format */

import "./signup_style.css";
import { useState } from "react";
import useFetch from "../../hooks/useFetch";
import { useHistory, Link } from "react-router-dom";
import { initialState, checkPassword } from "./signup_helpers";
export default function Signup() {
  let history = useHistory();
  const [state, setState] = useState(initialState);
  const handleChange = ({ target: { name, value } }) =>
    setState({ ...state, [name]: value });
  let { response, error } = useFetch("", state.performFetch, {
    method: "POST",
    body: JSON.stringify({
      username: state.username,
      password: state.password,
      email: state.email,
    }),
  });
  const handleClick = e => {};
  const handleSubmit = e => {
    e.preventDefault();
    console.log(e);
    checkPassword(state);
  };
  return (
    <div>
      <form onSubmit={handleSubmit} style={{ border: "1px solid #ccc" }}>
        <div className="container">
          <h1>Sign Up</h1>
          <p>Please fill in this form to create an account.</p>
          <hr />
          <label htmlFor="first_name">
            <b>First Name:</b>
          </label>
          <input
            onChange={handleChange}
            type="text"
            placeholder="First Name"
            name="first_Name"
          />

          <label htmlFor="middle_name">
            <b>Middle Name:</b>
          </label>
          <input
            onChange={handleChange}
            type="text"
            placeholder="Middle Name"
            name="middle_name"
          />

          <label htmlFor="last_name">
            <b>Last Name:</b>
          </label>
          <input
            onChange={handleChange}
            type="text"
            placeholder="Last Name"
            name="last_name"
          />
          <label htmlFor="username">
            <b>Email</b>
            <input onChange={handleChange} name="username" />
          </label>
          <label htmlFor="email">
            <b>Email</b>
          </label>
          <input
            onChange={handleChange}
            type="text"
            placeholder="Enter Email"
            name="email"
          />

          <label htmlFor="password">
            <b>Password</b>
          </label>
          <input
            onChange={handleChange}
            type="password"
            placeholder="Enter Password"
            name="password"
          />

          <label htmlFor="password_confirmation">
            <b>Repeat Password</b>
          </label>
          <input
            onChange={handleChange}
            type="password"
            placeholder="Repeat Password"
            name="password_confirmation"
          />

          <label>
            <input
              onChange={handleChange}
              type="checkbox"
              checked="checked"
              name="remember"
              style={{ marginBottom: "15px" }}
            />{" "}
            Remember me
          </label>

          <p>
            By creating an account you agree to our{" "}
            <a href="/signup" style={{ color: "dodgerblue" }}>
              Terms & Privacy
            </a>
            .
          </p>

          <div className="clearfix">
            <button
              type="button"
              onClick={() => history.push("/")}
              className="cancelbtn"
            >
              Cancel
            </button>
            <button type="submit" onClick={handleClick} className="signupbtn">
              Sign Up
            </button>
          </div>
        </div>
      </form>
      <Link to="/login">Already have an account?</Link>
    </div>
  );
}
