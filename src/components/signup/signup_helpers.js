/** @format */

async function postUserSignup() {}
export const initialState = {
  last_name: "DiCaprio",
  performFetch: false,
  username: "leonardo.wilhelm.dicaprio",
  email: "leonardo.wilhelm.dicaprio@famous.com",
  first_name: "Leonardo",
  password: "password123!@#",
  middle_name: "Wilhelm",
  password_confirm: "password123!@#",
};
export const checkPassword = _state => {
  let { password, password_confirm } = _state;
  if (password?.length !== password_confirm?.length) {
    return console.log("The passwords have different lengths");
  }
  for (let index = 0; index < password.length; index++) {
    if (password[index] !== password_confirm[index]) {
      return console.log("The passwords do not match");
    }
  }
};
