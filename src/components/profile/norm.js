/** @format */

import { readFileSync, writeFileSync } from "fs";

let data = JSON.parse(readFileSync("./normalized.json"));
let output = {};
for (let section in data) {
  let temp = data[section].map(user => {
    let boke = user[0].toLowerCase().split(" ");
    let bike = user[1]
      .toLowerCase()
      .replaceAll(".", "")
      .replaceAll("-", "_")
      .split(" ");
    switch (boke.length) {
      default:
        return {
          first_name: boke[0],
          middle_name: boke[1],
          last_name: boke[2],
          username: `${bike[0]}.${bike[1]}.${bike[3]}`,
          password: "password123!@#",
          get email() {
            return `${this.username}@${section}.com`;
          },
        };

      case 1:
        return {
          first_name: boke[0],
          middle_name: "",
          last_name: "",
          username: `${bike[0]}`,
          password: "password123!@#",
          get email() {
            return `${this.username}@${section}.com`;
          },
        };
      case 2:
        return {
          first_name: boke[0],
          middle_name: "",
          last_name: boke[1],
          username: `${bike[0]}.${bike[1]}`,
          password: "password123!@#",
          get email() {
            return `${this.username}@${section}.com`;
          },
        };
    }
  });

  output[section] = temp;
}

writeFileSync("./normalized2.json", JSON.stringify(output));
