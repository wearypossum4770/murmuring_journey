/** @format */
import { useState, useContext, useEffect } from "react";
import { useTodo } from "../../context/TodoContext";
import useFetch from "../../hooks/useFetch";
export default function Dashboard() {
  const [statistics, setStatistics] = useState({});
  const [todoList, setTodoList] = useState();
  const [hasRendered, setHasRendered] = useState(false);
  let today = new Date();
  let options = {
    headers: {
      Authorization: `Token ${authTokens}`,
    },
  };
  const createTodo = () => {
    setAppState({ type: "USE_LOCAL" });
  };
  let {
    appState: { fetchTodo, todos },
    setAppState,
  } = useTodo();
  let authTokens = "f6158acd6b2a72a97e1ed5deb75c22a82407837e";
  useEffect(() => {
    setTodoList(todos);
    setStatistics({
      completed: todos?.filter(todo => todo.completed === true).length,
      overdue: todos?.filter(
        todo =>
          new Date(todo.date_due).valueOf() < today.valueOf() &&
          !todo.completed,
      ).length,
      total: todos?.length,
      get overduePercentage() {
        return `${parseInt((this.overdue / this.total) * 100)}%`;
      },
      get completedPercent() {
        return `${parseInt((this.completed / this.total) * 100)}%`;
      },
    });
    setHasRendered(true);
  }, [todos]);
  useEffect(() => {
    async function getTodos() {
      let options = {
        mode: "cors",
        headers: {
          Authorization: `Token ${authTokens}`,
        },
      };
      try {
        const response = await fetch("http://127.0.0.1:8000/todos", options);
        if (response.ok) {
          const data = await response.json();
          localStorage.setItem("todo_list", JSON.stringify(data));
          setAppState({ type: "SET_TODOS", payload: data });
        }
      } catch (err) {
        console.log(err);
      }
    }
    if (fetchTodo) {
      getTodos();
    }
  }, []);
  return (
    <div className="w3-container">
      <h2>Progress Bar Width</h2>

      {hasRendered ? (
        <>
          <p>Completed: {statistics.completedPercent} &#177;1&#37; </p>
          <div className="w3-light-grey">
            <div
              className="w3-grey"
              style={{
                height: "24px",
                width: statistics.completedPercent,
              }}
            ></div>
          </div>
          <p>Overdue: {statistics.overduePercentage} &#177; 1&#37;</p>
          <div className="w3-light-grey">
            <div
              className="w3-grey"
              style={{
                height: "24px",
                width: statistics.overduePercentage,
              }}
            ></div>
          </div>
        </>
      ) : null}
      <br />
      {fetchTodo ? (
        <div>Loading...</div>
      ) : (
        todos.map(todo_key => (
          <div className="w3-card-4 w3-dark-grey">
            <div className="w3-container w3-center">
              <h3>Friend request</h3>
              <img
                src="img_avatar3.png"
                alt="Avatar"
                style={{ width: "80%" }}
              />
              <h5>{todo_key.content}</h5>
              <button className="w3-button w3-green">Mark as Completed</button>
              <button className="w3-button w3-red">Mark as Deleted</button>
            </div>
          </div>
        ))
      )}
    </div>
  );
}
