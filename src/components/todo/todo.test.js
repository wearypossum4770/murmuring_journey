/** @format */

// "http://127.0.0.1:8000/todos/"
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import TodoList from "./TodoList";
let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});
it("renders user's todolist", async () => {
  const fakeTodoList = [
    {
      pk: 20,
      title: "First Impeachment",
      owner: 15,
      content: "Get impeached by the United States House of Representatives",
      date_modified: "2021-05-24T20:52:26.585Z",
      date_created: "2021-05-24T20:52:26.568Z",
      date_due: "2020-02-05",
      date_completed: "2021-05-24T20:52:26.585Z",
      completed: true,
      is_archived: true,
      note: "",
    },
    {
      pk: 21,
      title: "Second Impeachment",
      owner: 15,
      content: "Get impeached by the United States House of Representatives",
      date_modified: "2021-05-24T20:52:26.621Z",
      date_created: "2021-05-24T20:52:26.602Z",
      date_due: "2021-02-13",
      date_completed: "2021-05-24T20:52:26.621Z",
      completed: true,
      is_archived: true,
      note: "",
    },
    {
      pk: 22,
      title: "Secret Pardon",
      owner: 15,
      content: "Give yourself a secret pardon",
      date_modified: "2021-05-24T20:52:26.659Z",
      date_created: "2021-05-24T20:52:26.640Z",
      date_due: "2021-02-13",
      date_completed: "2021-05-24T20:52:26.659Z",
      completed: true,
      is_archived: true,
      note: "The FBI is closing in on me and my businesses. I really need a pardon",
    },
  ];
  jest
    .spyOn(global, "fetch")
    .mockImplementation(() =>
      Promise.resolve({ json: () => Promise.resolve(fakeTodoList) }),
    );
  await act(async () => {
    render(<TodoList />, container);
  });
});
