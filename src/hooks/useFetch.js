/** @format */

import { useEffect, useReducer } from "react";
export default function useFetch(url = "", performFetch, options = {}) {
  let controller = new AbortController();
  let opts = {
    ...options,
    signal: controller.signal,
    mode: "cors",
    headers: { "Content-Type": "application/json" },
  };
  function reducer(state, action) {
    switch (action.type) {
      default:
        return { ...state };
      case "SUCCESS":
        return { ...state,  ...action.payload };
      case "ERROR":
        return { ...state, errors: action.payload };
    }
  }
  const [fetchReducer, setFetchReducer] = useReducer(reducer, {
    response: null,
    data: null,
    errors: null,
  });
  useEffect(() => {
    async function callAPI() {
      try {
        const response = await fetch(url, opts);
        if (response.ok) {
          let { statusText, status, ok, redirected } = response;
          setFetchReducer({
            type: "SUCCESS",
            payload: {
              response: { statusText, status, ok, redirected },
              data: await response.json(),
            },
          });
          controller = null;
        }
      } catch (err) {
        setFetchReducer({ type: "ERROR", payload: err });
      }
    }
    if (performFetch) {
      callAPI();
    }
    return () => controller?.abort();
  }, [url, opts, performFetch]);
  return fetchReducer;
}
