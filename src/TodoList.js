/** @format */

import { useState, useEffect } from "react";
import * as data from "";
// import APIFetch from "./APIFetch";
export default function TodoList() {
  let [todoList, setTodoList] = useState();
  //   let [display,setDisplay] = useState(true)
  let [errors, setErrors] = useState();

  useEffect(() => {
    let options = {
      mode: "cors",
    };
    async function UseFetch() {
      try {
        const resp = await (
          await fetch("http://127.0.0.1:8000/todos/", options)
        ).json();
        setTodoList(resp.todo_list);
      } catch (err) {
        setErrors(err);
      }
    }
    UseFetch();
  }, []);
  return (
    <div className="w3-container">
      <ul className="w3-ul w3-card-4">
        {todoList?.map(todo => (
          <li key={todo.id} className="w3-bar">
            <span className="w3-bar-item w3-button w3-white w3-xlarge w3-right">
              ×
            </span>
            <img
              src="img_avatar5.png"
              className="w3-bar-item w3-circle w3-hide-small"
              style={{ width: "85px" }}
            />
            <div className="w3-bar-item">
              <span className="w3-large">Jill</span>
              <br />
              <span>{todo.title}</span>
            </div>
          </li>
        ))}
      </ul>
    </div>
  );
}

/***
 *        {todoList.map(todo=>(
                 <li className="w3-bar">
                 <span onclick="this.parentElement.style.display='none'" className="w3-bar-item w3-button w3-white w3-xlarge w3-right">×</span>
                 <img src="img_avatar5.png" className="w3-bar-item w3-circle w3-hide-small" style="width:85px"/>
                 <div className="w3-bar-item">
                   <span className="w3-large">Jill</span><br/>
                   <span>Support</span>
                 </div>
               </li>
        ))}
 */
