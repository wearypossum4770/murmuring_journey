/** @format */
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
// import TodoList from "./TodoList";
import { useEffect, useReducer } from "react";
import { TodoContext } from "./context/TodoContext";
import { AuthContext } from "./context/AuthContext";
import Dashboard from "./components/dashboard/Dashboard";
import PrivateRoute from "./components/router/PrivateRoute";
import Signup from "./components/signup/Signup";
import Home from "./components/home/Home";
import Login from "./components/login/Login";
export default function App() {
  const initialState = {
    isLoggedIn: false,
    fetchTodo: true,
    isOnLine: navigator.onLine,
    authTokens: null,
    credentials: {},
    useLocalStorage: true,
    todos: [],
    hasRendered: false,
    todoStatistics: {
      completed: 0,
      overdue: 0,
      get overduePercentage() {},
      get completedPercent() {},
    },
  };
  function reducer(state, action) {
    switch (action.type) {
      default:
        return { ...state };
      case "USE_LOCAL":
        return [...state];
      case "SET_TOKENS":
        return { ...state, isLoggedIn: true, authTokens: action.payload };
      case "LOGOUT":
        return { ...state, authTokens: null };
      case "SET_TODOS":
        return { ...state, fetchTodo: false, todos: [action.payload] };
    }
  }
  const [appState, setAppState] = useReducer(reducer, initialState);
  const setTokens = data => {
    localStorage.setItem("tokens", JSON.stringify(data));
    setAppState({ type: "SET_TOKENS", payload: data });
    let todo_list = JSON.parse(localStorage.getItem("todo_list"));
    if (appState.todos.length > 0 && todo_list) {
      setAppState({ type: "SET_TODOS", payload: todo_list.todo_list });
    }
  };
  const logOut = () => setAppState({ type: "LOGOUT" });
  const nonce = () => {
    let todo_list = JSON.parse(localStorage.getItem("todo_list"));
    if (todo_list && !appState?.todos?.length) {
      setAppState({ type: "SET_TODOS", payload: todo_list.todo_list });
    }
  };
  useEffect(() => {
    nonce();
  }, []);
  const setTodoList = data => {
    setAppState({ type: "SET_TODOS", payload: data.todo_list });
  };
  return (
    <Router>
      <div>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          {appState.isLoggedIn ? (
            <>
              <li>
                <Link to="/dashboard">Dashboard</Link>
              </li>
              <li>
                <a href="/">Log out</a>
              </li>
            </>
          ) : (
            <>
              <li>
                <Link to="/signup">Signup</Link>
              </li>
              <li>
                <Link to="/login">Login</Link>
              </li>
            </>
          )}
        </ul>
        <hr />
        <Switch>
          <AuthContext.Provider
            value={{
              setAuthTokens: setTokens,
              logOut,
              authTokens: appState?.authTokens,
              setAppState,
            }}
          >
            <Route exact path="/login" component={Login} />
            <Route exact path="/" component={Home} />
            <Route exact path="/signup" component={Signup} />
            <TodoContext.Provider value={{ appState, setAppState }}>
              <PrivateRoute exact path="/dashboard" component={Dashboard} />
              {/* <PrivateRoute exact path="/todos" component={TodoList} /> */}
            </TodoContext.Provider>
          </AuthContext.Provider>
        </Switch>
      </div>
    </Router>
  );
}
