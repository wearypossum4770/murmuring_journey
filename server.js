/** @format */

import express from "express";
import cors from "cors";
import path from "path";

let db = [
  {
    username: "john.doe",
    password: "password123!@#",
    auth_tokens: {
      access:
        "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwidXNlcm5hbWUiOiJqb2huLmRvZSIsImFkbWluIjpmYWxzZSwianRpIjoiZTY1YzBhNWQtYjdkMC00MzcxLTgxZmMtMTA1NjdiNTI5ZWI4IiwiaWF0IjoxNjIxODgyMDc3LCJleHAiOjE2MjE4ODU2OTh9.qNJxUzE2_gp3sJ-kalDnCsscuNgQ5Lki_t9txTatdQw",
      refresh:
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX3BrIjoxLCJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImNvbGRfc3R1ZmYiOiLimIMiLCJleHAiOjIzNDU2NywianRpIjoiZGUxMmY0ZTY3MDY4NDI3ODg5ZjE1YWMyNzcwZGEwNTEifQ.aEoAYkSJjoWH1boshQAaTkf8G3yn0kapko6HFRt7Rh4",
    },
  },
  {
    username: "jane.doe",
    password: "password123!@#",
    auth_tokens: {
      access:
        "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwidXNlcm5hbWUiOiJqYW5lLmRvZSIsImFkbWluIjpmYWxzZSwianRpIjoiZTY1YzBhNWQtYjdkMC00MzcxLTgxZmMtMTA1NjdiNTI5ZWI4IiwiaWF0IjoxNjIxODgyMDc3LCJleHAiOjE2MjE4ODU3MzV9.BNXOnfBQiu6IBJx6RcQY3TbejyhI63R_anNnW4y4Kkg",
      refresh:
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX3BrIjoxLCJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImNvbGRfc3R1ZmYiOiLimIMiLCJleHAiOjIzNDU2NywianRpIjoiZGUxMmY0ZTY3MDY4NDI3ODg5ZjE1YWMyNzcwZGEwNTEifQ.aEoAYkSJjoWH1boshQAaTkf8G3yn0kapko6HFRt7Rh4",
    },
  },
];
let users = {
  login: function (_username, _password) {
    return db.filter(
      user => user.username === _username && user.password === _password,
    );
  },
};
import { readFile, writeFile } from "fs";
import { fileURLToPath } from "url";
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const PORT = 3003;
const app = express();
app.use(cors());

app.use(express.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, "static")));

app.get("/", (req, res) => {});
app.post("/api/token/", (req, res) => {
  console.log(req.body);
  console.log(req.params);
  console.log(req.data);
  res.json({});
});

app.listen(PORT, () => {
  console.log(`Example app listening at http://localhost:${PORT}`);
});
