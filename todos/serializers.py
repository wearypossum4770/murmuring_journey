from rest_framework.serializers import ModelSerializer

from todos.models import Todo


class TodoSerializer(ModelSerializer):
    class Meta:
        model = Todo
        fields = (
            "title",
            "content",
            "date_due",
            "completed",
            "is_archived",
            "note",
            "date_completed",
            "is_due",
            "is_overdue",
        )
