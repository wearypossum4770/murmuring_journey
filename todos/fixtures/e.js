/** @format */

import { readFileSync, writeFileSync } from "fs";
let data = JSON.parse(readFileSync("./todos/fixtures/todoinit.json"));

let users = data.map((user, index) => {
  if (user["model"] === "todos.todo") {
    user["pk"] = index;
    delete user["date_modified"];
    delete user["date_created"];
    delete user["date_due"];
    delete user["date_completed"];
    delete user["completed"];
    delete user["is_archived"];
    user["fields"]["date_modified"] = "2021-05-20T00:34:07.359Z";
    user["fields"]["date_created"] = "2021-05-20T00:34:07.359Z";
    user["fields"]["date_due"] = "2021-05-20";
    user["fields"]["date_completed"] = "2021-05-20T00:34:07.359Z";
    user["fields"]["completed"] = true;
    user["fields"]["is_archived"] = true;
  }
  return user;
});
writeFileSync("./todos/fixtures/todoinit.json", JSON.stringify(users));
writeFileSync("./users/fixtures/todoinit.json", JSON.stringify(users));
console.log(users[5]);
