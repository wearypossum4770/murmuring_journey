from datetime import date

from django.contrib.auth import get_user_model
from django.db.models import (
    CASCADE,
    BooleanField,
    CharField,
    DateField,
    DateTimeField,
    ForeignKey,
    ManyToManyField,
    Model,
    TextField,
)
from django.utils import timezone

User = get_user_model()


class Todo(Model):
    title = CharField(max_length=80)
    owner = ForeignKey(User, on_delete=CASCADE)
    content = CharField(max_length=100)
    date_modified = DateTimeField(auto_now=True)
    date_created = DateTimeField(auto_now_add=True)
    date_due = DateField(null=True, blank=True)
    date_completed = DateTimeField(null=True, blank=True)
    completed = BooleanField(default=False)
    is_archived = BooleanField(default=False)
    note = TextField(default="", null=True, blank=True)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if self.completed:
            self.date_completed = timezone.now()
        super().save()

    @property
    def is_due(self, *args, **kwargs):
        return date.today() == self.date_due

    @property
    def is_overdue(self, *args, **kwargs):
        if self.date_due:
            return date.today() > self.date_due
