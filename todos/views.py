from django.http import JsonResponse
from django.shortcuts import get_object_or_404, render
from rest_framework.authentication import (
    BasicAuthentication,
    SessionAuthentication,
    TokenAuthentication,
)
from rest_framework.decorators import (
    api_view,
    authentication_classes,
    permission_classes,
)
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ViewSet

from todos.models import Todo
from todos.serializers import TodoSerializer


# has_object_permission
class TodoListViewSet(ViewSet):
    def get_queryset(self, request):
        user = self.request.user.id
        todos = Todo.objects.filter(owner=user)
        todo_list = [
            {
                "id": key,
                "title": todo.title,
                "content": todo.content,
                "date_due": todo.date_due.__str__(),
                "date_completed": todo.date_completed.__str__(),
                "completed": todo.completed,
                "is_archived": todo.is_archived,
                "note": todo.note,
                "is_due": todo.is_due,
                "is_overdue": todo.is_overdue,
            }
            for key, todo in enumerate(todos)
        ]
        return todo_list

    def list(self, request):
        queryset = self.get_queryset(request)
        serializer = TodoSerializer(queryset, many=True)
        return JsonResponse({"todo_list": serializer.data})

    # def get_queryset(self):
    #     return self.request.user.todo


@api_view(["GET"])
@permission_classes([IsAuthenticated])
@authentication_classes(
    [TokenAuthentication, SessionAuthentication, BasicAuthentication]
)
def api_index(request):
    todos = Todo.objects.all()
    todo_list = [
        {
            "id": key,
            "title": todo.title,
            "content": todo.content,
            "date_due": todo.date_due.__str__(),
            "date_completed": todo.date_completed.__str__(),
            "completed": todo.completed,
            "is_archived": todo.is_archived,
            "note": todo.note,
            "is_due": todo.is_due,
            "is_overdue": todo.is_overdue,
        }
        for key, todo in enumerate(todos)
    ]
    content = {
        "user": str(request.user),  # `django.contrib.auth.User` instance.
        "auth": str(request.auth),  # None
        "todo_list": todo_list,
    }
    return JsonResponse(content)


def api_detail(request):
    ...


def api_update(request):
    ...


@api_view(["POST"])
# @permission_classes(...)
# @authentication_classes(...)
def api_create(request):
    if request.method == "POST":
        todo = Todo.object.create(request.data)
    return render(request)


def index(request):
    todos = Todo.objects.all()
    todo_list = []
    for todo in todos:
        todo_list.append(
            {
                "title": todo.title,
                "content": todo.content,
                "date_due": todo.date_due.__str__(),
                "date_completed": todo.date_completed.__str__(),
                "completed": todo.completed,
                "is_archived": todo.is_archived,
                "note": todo.note,
                "is_due": todo.is_due,
                "is_overdue": todo.is_overdue,
            }
        )
    return render(request, "todo/index.html", {"todo_list": todo_list})
