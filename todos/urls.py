from django.urls import path
from rest_framework.routers import DefaultRouter

from todos.views import TodoListViewSet, api_create, api_index

router = DefaultRouter()
router.register("", TodoListViewSet, basename="todo")
urlpatterns = [
    # path("", api_index, name="api-index"),
    path("new/", api_create, name="api-create"),
]

urlpatterns += router.urls
