from datetime import date, datetime, timezone

import pytest
from django.contrib.auth import get_user_model
from django.test import Client, TestCase

from todos.models import Todo
from users.models import Profile

pytestmark = pytest.mark.django_db


class TestTodo(TestCase):
    fixtures = ("todoinit.json",)

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.client = Client()

        cls.todo_with_attachemnt = Todo.objects.get(pk=21)
        cls.tump_todo = Todo.objects.filter(
            owner=get_user_model().objects.get(username="donald.john.tump.sr")
        )
        cls.todo_list = Todo.objects.all()
        cls.todo = Todo.objects.get(pk=1)
        cls.dt = datetime(2021, 5, 20, 0, 34, 7, 359000, tzinfo=timezone.utc)

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def test_something(self):
        assert True == True

    def test_todo_list(self):
        assert len(self.todo_list) > 0

    def test_title(self):
        assert self.todo.title == "ziplining"

    def test_content(self):
        assert self.todo.content == "Take the family and go ziplining"

    def test_completed(self):
        assert self.todo.completed == True

    def test_is_archived(self):
        assert self.todo.is_archived == True

    def test_date_modified(self):
        assert self.todo.date_modified == self.dt

    def test_date_created(self):
        self.todo.date_created == self.dt

    def test_date_due(self):
        assert self.todo.date_due == date(2021, 5, 20)

    def test_date_completed(self):
        assert self.todo.date_completed == self.dt

    def test_date_modified(self):
        self.todo.date_modified == self.dt

    def query_set_faker(self, queryset):
        return [
            {
                "id": key,
                "title": todo.title,
                "content": todo.content,
                "date_due": todo.date_due.__str__(),
                "date_completed": todo.date_completed.__str__(),
                "completed": todo.completed,
                "is_archived": todo.is_archived,
                "note": todo.note,
                "is_due": todo.is_due,
                "is_overdue": todo.is_overdue,
            }
            for key, todo in enumerate(queryset)
        ]

    def test_note(self):
        assert (
            self.todo_with_attachemnt.note
            == "The FBI is closing in on me and my businesses. I really need a pardon"
        )

    def test_todo_object_permissions(self):
        todo_list = self.query_set_faker(self.tump_todo)
        assert len(todo_list) == 3
