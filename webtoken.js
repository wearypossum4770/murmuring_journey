/** @format */

// https://datatracker.ietf.org/doc/html/rfc7643#section-4.1.2
let token = {
  honorificSuffix: "Jr.",
  honorificPrefix: "Dr.",
  middleName: "Luther",
  givenName: "Martin",
  familyName: "King",
  get fullName() {
    return `${this.honorificPrefix} ${this.givenName} ${this.middleName} ${this.familyName} ${this.honorificSuffix}`;
  },
  displayName: "Dr. King",
  nickname: "Dr. King",
  profileUrl: "",
  title: "Civil Rights Leader",
  userType: "",
  id: 1,
  externalId: "",
  preferredLanguage: "",
  username: "",
  locale: "",
  timezone: "",
  isActive: true,
  addresses: [
    {
      streetAddress1: "",
      streetAddress2: "",
      city: "",
      region: "",
      postalCode: "",
      country: "",
      type: "home",
    },
  ],
  password: "password123!@#",
  entitlements: ["todo", "profile", "user"],
  roles: ["admin", "staff", "regular"],
  x509Certificates: ["./example.com.csr"],
  enterprise: {
    manager: "",
    employeeNumber: "",
    costCenter: "",
    organization: "",
    division: "",
    department: "",
  },
};
