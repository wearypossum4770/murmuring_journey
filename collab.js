/** @format */

// Problem 1: serialize binary tree

// Problem 2: deserialize binary tee from string

const tree = {
  val: 2,
  left: {
    val: 3,
    left: {
      val: 4,
    },
    right: {
      val: 5,
    },
  },
  right: {
    val: 6,
    left: {
      val: 7,
    },
  },
};

// 2(3(4)(5))(6(7))

const serialize = input => {
  let target = `${input?.val}`;
  if (input?.left) {
    target += `(${serialize(input?.left)}`;
  }
  if (input?.right) {
    target += `)(${serialize(input?.right)})`;
  }
  return target;
};

console.log(serialize(tree));
// https://adfinis.com/en/blog/openssl-x509-certificates/
